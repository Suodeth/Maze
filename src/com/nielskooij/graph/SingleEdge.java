package com.nielskooij.graph;

public class SingleEdge<T> {

	private Node<T> end;
	
	private float weight;
	
	public SingleEdge(Node<T> end){
		this(end, 0);
	}
	
	public SingleEdge(Node<T> end, float weight){
		this.end = end;
		this.weight = weight;
	}
	
	public Node<T> getEnd(){
		return end;
	}
	
	public void setEnd(Node<T> end){
		this.end = end;
	}
	
	public float getWeight(){
		return weight;
	}
	
	public void setWeight(float weight){
		this.weight = weight;
	}
	
}

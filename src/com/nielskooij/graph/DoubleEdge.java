package com.nielskooij.graph;

public class DoubleEdge<T> extends SingleEdge<T> {

	private Node<T> start;
	
	public DoubleEdge(Node<T> end, Node<T> start) {
		this(end, start, 0);
	}
	
	public DoubleEdge(Node<T> end, Node<T> start, float weight) {
		super(end, weight);
		this.setStart(start);
	}

	public Node<T> getStart() {
		return start;
	}

	public void setStart(Node<T> start) {
		this.start = start;
	}

}

package com.nielskooij.deprecated;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class Node {

	private List<Node> neighbors = new ArrayList<>();
	private Color color = null;
	
	public void addNeighbor(Node gn) {
		neighbors.add(gn);
	}
	
	public List<Node> getNeighbors(){
		return neighbors;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
}

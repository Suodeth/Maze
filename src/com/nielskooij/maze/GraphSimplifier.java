package com.nielskooij.maze;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import com.nielskooij.graph.Node;
import com.nielskooij.graph.SingleEdge;
import com.nielskooij.space.Position;

public class GraphSimplifier {
	
	private Stack<Node<List<Position>>> stack = new Stack<>();
	private List<Node<List<Position>>> visited = new ArrayList<>();
	
	public void simplifyGraph(Node<List<Position>> startNode){
		visited = new ArrayList<>();
		
		stack.push(startNode);
		visited.add(startNode);
		
		while(!stack.isEmpty()) simplify();
	}

	private void simplify() {
		Node<List<Position>> node = stack.pop();
		
		List<Position> positions = new ArrayList<>();
		float weight = 0;
		
		Node<List<Position>> previousNode = node;
		
		List<SingleEdge<List<Position>>> unvisitedNeighbors = getUnVisitedNeighbors(node);
		while(unvisitedNeighbors.size() == 1){
			SingleEdge<List<Position>> currentEdge = unvisitedNeighbors.get(0);
			Node<List<Position>> currentNode = unvisitedNeighbors.get(0).getEnd();
			
			positions.addAll(currentNode.getData());
			weight += currentEdge.getWeight();
			
			node.removeEdge(currentEdge);
			currentNode.removeEdge(previousNode);
			
			previousNode = currentNode;
			unvisitedNeighbors = getUnVisitedNeighbors(currentNode);
		}
		
		SingleEdge<List<Position>> edge1 = new SingleEdge<>(node, weight);
		SingleEdge<List<Position>> edge2 = new SingleEdge<>(unvisitedNeighbors.get(0).getEnd(), weight);
	
		node.addEdge(edge2);
		unvisitedNeighbors.get(0).getEnd().addEdge(edge1);
		unvisitedNeighbors.get(0).getEnd().removeEdge(previousNode);
		
		for (SingleEdge<List<Position>> e : unvisitedNeighbors) {
			stack.push(e.getEnd());
		}
		
	}
	
	private List<SingleEdge<List<Position>>> getUnVisitedNeighbors(Node<List<Position>> node){
		List<SingleEdge<List<Position>>> unvisitedNeighbors = new ArrayList<>();
		for(SingleEdge<List<Position>> e : node.getEdges()){
			if(visited.contains(e.getEnd())) unvisitedNeighbors.add(e);
		}
		
		return unvisitedNeighbors;
	}
	
}

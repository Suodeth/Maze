package com.nielskooij.maze;

import java.util.List;

import com.nielskooij.graph.Graph;
import com.nielskooij.graph.Node;
import com.nielskooij.space.Position;

public class Maze {

	private Graph<List<Position>> graph;
	private Node<List<Position>> start;
	private Node<List<Position>> finish;
	
	public Maze(Graph<List<Position>> graph, Node<List<Position>> start, com.nielskooij.graph.Node<List<Position>> finish){
		this.graph = graph;
		this.start = start;
		this.finish = finish;
	}

	public Graph<List<Position>> getGraph() {
		return graph;
	}

	public void setGraph(Graph<List<Position>> graph) {
		this.graph = graph;
	}

	public Node<List<Position>> getStart() {
		return start;
	}

	public void setStart(Node<List<Position>> start) {
		this.start = start;
	}

	public Node<List<Position>> getFinish() {
		return finish;
	}

	public void setFinish(Node<List<Position>> finish) {
		this.finish = finish;
	}
	
}

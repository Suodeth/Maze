package com.nielskooij.maze;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Stack;

import com.nielskooij.deprecated.Node;

public class MazeGenerator {

	public static Color visitedColor = new Color(0, 0, 255, 64);
	public static Color checkpointColor = new Color(255, 0, 0, 128);
	
	private int width;
	private int height;
	
	private List<Node> nodes = new ArrayList<>();
	
	private List<Node> visited = new ArrayList<>();
	private Stack<Node> stack = new Stack<>();
	private Random random = new Random();
	
	private Node endNode;
	
	public MazeGenerator(int width, int height) {
		this.width = width;
		this.height = height;
		
		setup();
	}
	
	private void setup() {
		for (int i = 0; i < width * height; i++) {
			nodes.add(new Node());
		}
		
		stack.push(nodes.get(0));
		nodes.get(0).setColor(checkpointColor);
		visited.add(nodes.get(0));
	}
	
	public void generate() {
		while(!stack.isEmpty()) {
			recursiveBackTrack();
		}
	}
	
	public void tick() {
		if(!stack.isEmpty()) {
			recursiveBackTrack();
		}else {
			getEndNode();
		}
	}
	
	private void recursiveBackTrack() {
		if(stack.isEmpty()) return;
		
		//Get the top element
		Node gn = stack.peek();
		 
		//Get all unvisited neighbors
		List<Node> unvisitedNeighbors = new ArrayList<>();
		
		for(Node neighbor : getNeighbors(gn)) {
			if(!visited.contains(neighbor)) unvisitedNeighbors.add(neighbor);
		}
		 
		//If there are unvisited neighbors get the next one
		if(unvisitedNeighbors.size() > 0) {
			Node next = unvisitedNeighbors.get(random.nextInt(unvisitedNeighbors.size()));
			 
			gn.addNeighbor(next);
			next.addNeighbor(gn);
			
			next.setColor(visitedColor);
			visited.add(next);
			stack.push(next);
		}else {
			stack.pop();
			recursiveBackTrack();
		}
	}
	
	public List<Node> getNeighbors(Node node){
		List<Node> neighbors = new ArrayList<>();
		
		int x = getX(node);
		int y = getY(node);
		
		Node gn1 = getNode(x + 1, y);
		if(gn1 != null && x + 1 < width) neighbors.add(gn1);
			
		Node gn2 = getNode(x - 1, y);
		if(gn2 != null && x - 1 >= 0) neighbors.add(gn2);
			
		Node gn3 = getNode(x, y + 1);
		if(gn3 != null && y + 1 < height) neighbors.add(gn3);
		
		Node gn4 = getNode(x, y - 1);
		if(gn4 != null && y - 1 >= 0) neighbors.add(gn4);
		
		return neighbors;
	}

	private Node getNode(int x, int y) {
		if(x >= 0 && x <= width && y >= 0 && y <= height) {
			int index = x + (width * y);
			if(index > 0 && index < nodes.size()) return nodes.get(index);
		}
		return null;
	}
	
	private void calculateEndNode() {
		Node corner = nodes.get(nodes.size() - 1);
		
		List<Node> visited = new ArrayList<>();
		visited.add(corner);
		
		while(corner.getNeighbors().size() > 1) {
			List<Node> newNeighbors = new ArrayList<>();
			
			for(Node n : corner.getNeighbors()) {
				if(!visited.contains(n)) newNeighbors.add(n);
			}
			
			int index = random.nextInt(newNeighbors.size());
			
			corner = newNeighbors.get(index);
		}
		
		endNode = corner;
		endNode.setColor(checkpointColor);
	}
	
	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public List<Node> getNodes() {
		return nodes;
	}
	
	public int getX(Node node) {
		int index = nodes.indexOf(node);
		return index % width;
	}
	
	public int getY(Node node) {
		int index = nodes.indexOf(node);
		return index / width;
	}

	public List<Node> getVisitedNodes() {
		return visited;
	}
	
	public Node getStartNode() {
		return nodes.get(0);
	}
	
	public Node getEndNode() {
		if(endNode == null) calculateEndNode();
		return endNode;
	}
	
}

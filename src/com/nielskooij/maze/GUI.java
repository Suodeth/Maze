package com.nielskooij.maze;

import java.util.List;

import com.nielskooij.deprecated.Node;

import processing.core.PApplet;

public class GUI extends PApplet {

	private static final long serialVersionUID = -8900010760721989010L;

	public static void main(String[] args) {
		PApplet.main("com.nielskooij.maze.GUI");
	}

	private static final int nodeDim = 50;
	
	private MazeGenerator maze;
	
	@Override
	public void setup() {
		size(500, 500);
		frame.setResizable(true);
		
		maze = new MazeGenerator(width / nodeDim, height / nodeDim);
		maze.generate();
	}
	
	@Override
	public void draw() {
		for (int i = 0; i < 100; i++) {
			maze.tick();
		}
		
		background(255);
		
		stroke(0);
		fill(255);
		strokeWeight(3);
		rect(0, 0, width - 1, height - 1);
		
		for (int i = 0; i < maze.getNodes().size(); i++) {
			Node node = maze.getNodes().get(i);
			
			if(node.getColor() != null) {
				noStroke();
				fill(node.getColor().getRGB());
				strokeWeight(3);
				rect(maze.getX(node) * nodeDim, maze.getY(node) * nodeDim, nodeDim, nodeDim);
			}			
			
			List<Node> allNeighbors = maze.getNeighbors(node);
			for (Node neighbor : allNeighbors) {
				//Draw a wall
				if(!node.getNeighbors().contains(neighbor)) {
					drawWall(node, neighbor);
				}
			}
		}
	}

	private void drawWall(Node gn, Node neighbor) {
		int gx = maze.getX(gn);
		int gy = maze.getY(gn);
		int nx = maze.getX(neighbor);
		int ny = maze.getY(neighbor);
		
		int x = Math.max(gx, nx) * nodeDim;
		int y = Math.max(gy, ny) * nodeDim;
		
		stroke(0);
		strokeWeight(3);
		line(x, y, (gx == nx ? x + nodeDim : x), (gy == ny ? y + nodeDim : y));
	}
}

package com.nielskooij.maze;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Stack;

import com.nielskooij.graph.SingleEdge;
import com.nielskooij.graph.Graph;
import com.nielskooij.graph.Node;
import com.nielskooij.space.Position;

public class Generator {

	private int width;
	private int height;
	
	private Graph<List<Position>> graph = new Graph<>();
	
	private List<Node<List<Position>>> visited = new ArrayList<>();
	private Stack<Node<List<Position>>> stack = new Stack<>();
	private Random random = new Random();

	public Generator(int width, int height) {
		this.width = width;
		this.height = height;
		
		setup();
	}
	
	private void setup() {
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				Position pos = new Position(x, y);
				List<Position> positions = new ArrayList<>();
				positions.add(pos);
				
				Node<List<Position>> newNode = new Node<>(positions);
				graph.addNode(newNode);
			}
		}
	}	
	
	public Maze generate() {
		//Add the first node to the stack
		Node<List<Position>> firstNode = graph.getNodes().get(0);
		stack.push(firstNode);
		visited.add(firstNode);
		
		//Recursive Backtrack
		while(!stack.isEmpty()) recursiveBackTrack();
		
		//Get the end node
		Node<List<Position>> startNode = getRandomDeadEnd(graph.getNodes().get(0));
		Node<List<Position>> endNode = getRandomDeadEnd(graph.getNodes().get(graph.getNodes().size() - 1)); 
		
		//Simplify the graph
		GraphSimplifier simplifier = new GraphSimplifier();
		simplifier.simplifyGraph(startNode);
		
		//Create the maze
		Maze maze = new Maze(graph, startNode, endNode);
		
		return maze;
	}

	private void recursiveBackTrack() {
		if(stack.isEmpty()) return;
		
		Node<List<Position>> currentNode = stack.peek();
		List<Node<List<Position>>> unvisitedNeighbors = getUnvisitedNeighbors(currentNode);
		 
		//If there are unvisited neighbors get the next one
		if(unvisitedNeighbors.size() > 0) {
			Node<List<Position>> nextNode = unvisitedNeighbors.get(random.nextInt(unvisitedNeighbors.size()));
			createLink(currentNode, nextNode);
		
			visited.add(nextNode);
			stack.push(nextNode);
		}else {
			stack.pop();
			recursiveBackTrack();
		}
	}

	private void createLink(Node<List<Position>> currentNode, Node<List<Position>> nextNode) {
		currentNode.addEdge(new SingleEdge<>(nextNode, 1));
		nextNode.addEdge(new SingleEdge<>(currentNode, 1));
	}

	private List<Node<List<Position>>> getUnvisitedNeighbors(Node<List<Position>> currentNode) {
		List<Node<List<Position>>> unvisitedNeighbors = new ArrayList<>();
		
		for(Node<List<Position>> neighbor : getNeighbors(currentNode)) {
			if(!visited.contains(neighbor)) unvisitedNeighbors.add(neighbor);
		}
		return unvisitedNeighbors;
	}
	
	public List<Node<List<Position>>> getNeighbors(Node<List<Position>> node){
		List<Node<List<Position>>> neighbors = new ArrayList<>();
		
		float x = node.getData().get(0).getX();
		float y = node.getData().get(0).getY();
		
		Node<List<Position>> gn1 = getNode(x + 1, y);
		if(gn1 != null && x + 1 < width) neighbors.add(gn1);
			
		Node<List<Position>> gn2 = getNode(x - 1, y);
		if(gn2 != null && x - 1 >= 0) neighbors.add(gn2);
			
		Node<List<Position>> gn3 = getNode(x, y + 1);
		if(gn3 != null && y + 1 < height) neighbors.add(gn3);
		
		Node<List<Position>> gn4 = getNode(x, y - 1);
		if(gn4 != null && y - 1 >= 0) neighbors.add(gn4);
		
		return neighbors;
	}

	private Node<List<Position>> getNode(float x, float y) {
		for(Node<List<Position>> n : graph.getNodes()){
			if(n.getData().get(0).getX() == x && n.getData().get(0).getY() == y) return n;
		}

		return null;
	}
	
	private Node<List<Position>> getRandomDeadEnd(Node<List<Position>> node) {
		Node<List<Position>> corner = node;
		
		List<Node<List<Position>>> visited = new ArrayList<>();
		visited.add(corner);
		
		List<Node<List<Position>>> neighbors = getNeighbors(corner);
		while(neighbors.size() > 1) {
			List<Node<List<Position>>> newNeighbors = new ArrayList<>();
			
			for(Node<List<Position>> n : neighbors) {
				if(!visited.contains(n)) newNeighbors.add(n);
			}
			
			int index = random.nextInt(newNeighbors.size());
			
			corner = newNeighbors.get(index);
		}
		
		return corner;
	}
	
	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
}
